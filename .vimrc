set mouse=a
set term=xterm-256color
syntax on
set nu
set relativenumber
" No bleeping please
set visualbell
set hidden
set nohlsearch
set scrolloff=4

" Wrap
set wrap linebreak
set wrapmargin=0
set tw=100

set viminfo='20,<1000,s1000
set history=100
"Don't really need this but my term connection is fast so why not
set ttyfast
set undolevels=1000
" set spell spelllang=en_gb

" Copy to primary selection rather than system clipboard
vnoremap <C-c> "*y :let @+=@*<CR>
" Paste
map <C-v> "+p

" Change comment colour from the hard to read blue to a lighter shade.
highlight Comment ctermfg=26
highlight Visual ctermfg=yellow

" Theme
" colorscheme jellybeans
set background=dark


set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent


so ~/.vim/plugins.vim
map <C-o> :NERDTreeToggle<CR>
map <C-u> :UndotreeToggle<CR>

"new in vim 7.4.1042
let g:word_count=wordcount().words
function WordCount()
    if has_key(wordcount(),'visual_words')
        let g:word_count=wordcount().visual_words."/".wordcount().words " count selected words
    else
        let g:word_count=wordcount().cursor_words."/".wordcount().words " or shows words 'so far'
    endif
    return g:word_count
endfunction
set statusline+=\ w:%{WordCount()},
set laststatus=2 "show the status line
