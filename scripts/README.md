## backup
`backup` is a simple script to backup system folders to another folder, is mainly used for my Gentoo system.

## pullall
`pullall` is a script that will look through all folders and subfolders in the current directory. If the folder is a git folder it will fetch and pull the latest changes.
## save
`save` simply does a git add ., then git commit and git push.
